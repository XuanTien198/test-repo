#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
using namespace std;
#define Min 0.000001

struct monomial
{
	int sobien = 1;
	float coe;
	char *var;
	unsigned short *exp;
	monomial *pNext;
	monomial()
	{}
	monomial(monomial &a)
	{
		coe = a.coe;
		exp = a.exp;
		var = a.var;
		sobien = a.sobien;
	}
	monomial &operator=(monomial a)
	{
		monomial tmp(a);
		tmp.pNext = a.pNext;
		return tmp;
	}
	monomial operator+(monomial a)
	{
		monomial tmp;
		tmp.coe = coe + a.coe;
		tmp.exp = a.exp;
		tmp.var = a.var;
		return tmp;
	}
	monomial operator-(monomial a)
	{
		monomial tmp;
		tmp.coe = coe - a.coe;
		tmp.exp = a.exp;
		tmp.var = a.var;
		return tmp;
	}
	monomial operator*(monomial a)
	{
		monomial tmp = a; // luu a 
		tmp.coe *= coe;
		int m, n;
		m = strlen(a.var);
		n = strlen(var);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
				if (a.var[i] == var[j]) // gap bien giong nhau 
				{
					tmp.exp[i] += exp[j]; // cap nhat mu 
				}
		}
		int count = -1;
		for (int i = 0; i < n; i++)
		{
			bool test = false;
			for (int j = 0; j < m; j++)
				if (var[i] == a.var[j])
					test = true; // bien cua b da ton tai trong don thuc moi 
			if (!test) // khong co bien cua b trong don thuc moi  
			{
				count++;
				tmp.var[m + count] = var[i];
				tmp.exp[m + count] = exp[i];
			}
		}
		return tmp;
	}
};

class Polymial
{
public:
	void add2Poly(Polymial l1, Polymial l2);//add 2 Polymials	  
	void enqueue(monomial data);//enqueue a monomial into polynomial
	void shorten();
	void sortPoly();
	void createMono(monomial data);

private:
	monomial *pHead;
	monomial *pTail;
}

//create a monomial node
monomial *Polymial::createMono(monomial data)
{
	monomial *tmp = new monomial;
	tmp->coe = data.coe;
	tmp->var = data.var;
	tmp->exp = data.exp;
	tmp->pNext = NULL;
	tmp->sobien = data.sobien;
	return tmp;
}

//enqueue a monomial into polynomial
void Polymial::enqueue(monomial data)
{
	monomial *tmp = createMono(data);
	if (pHead == NULL)
	{
		pHead = pTail = tmp;
	}
	else
	{
		pTail->pNext = tmp;
		pTail = tmp;
		pTail->pNext = NULL;
	}
}
//add 2 polynomials
void Polymial::add2Poly(Polymial l1, Polymial l2)
{

	monomial *tmp1 = l1.pHead;
	monomial *tmp2 = l2.pHead;
	while (tmp1)
	{
		enqueue(*tmp1);
		tmp1 = tmp1->pNext;
	}
	while (tmp2 != NULL)
	{
		enqueue(*tmp2);
		tmp2 = tmp2->pNext;
	}
	shorten();
	sortPoly();
}